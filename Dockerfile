FROM gcr.io/distroless/java:11-debug
COPY build/libs/mn-eess-*-all.jar mn-eess.jar
EXPOSE 8080
CMD java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar mn-eess.jar
