package telegram

import io.micronaut.http.MediaType
import io.micronaut.http.client.multipart.MultipartBody

class SendVoice {
    String chat_id
    String caption
    byte[] bytes


    MultipartBody multipartBody(){
        return MultipartBody.builder()
                .addPart("chat_id", chat_id)
                .addPart("caption", caption ?:'')
                .addPart('voice', caption ?: 'instrucciones.ogg', MediaType.APPLICATION_OCTET_STREAM_TYPE, bytes)
                .build()
    }

}
