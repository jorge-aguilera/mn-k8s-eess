package telegram
import groovy.transform.ToString

@ToString(includeNames = true)
class InlineKeyboardMarkup extends ReplyMarkup{

    List<List<InlineKeyboardButton>> inline_keyboard = []

}
