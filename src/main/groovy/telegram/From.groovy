package telegram

import groovy.transform.ToString

@ToString
class From {
    int id
    String first_name
    String last_name
    String username
}
