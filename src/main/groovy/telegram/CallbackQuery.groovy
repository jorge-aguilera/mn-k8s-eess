package telegram
import groovy.transform.ToString

@ToString(includeNames = true)
class CallbackQuery {

    String id
    User from
    UserMessage message
    String inline_message_id
    String chat_instance
    String data
    String game_short_name

}
