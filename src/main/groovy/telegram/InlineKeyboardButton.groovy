package telegram
import groovy.transform.ToString

@ToString(includeNames = true)
class InlineKeyboardButton {
    String text
    String url
    String login_url
    String callback_data
    String switch_inline_query
    String switch_inline_query_current_chat
}
