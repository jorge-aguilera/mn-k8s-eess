package telegram

import groovy.transform.ToString

@ToString(includeNames = true)
class Message {
    String chat_id
    String text
    String parse_mode = 'markdown'
    Location location
    ReplyMarkup reply_markup
}
