package telegram

import groovy.transform.ToString

@ToString(includeNames = true)
class UserMessage {

    int message_id

    From from

    Chat chat

    long date

    String text

    Location location
}
