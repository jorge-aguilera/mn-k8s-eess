package telegram
import groovy.transform.ToString

@ToString(includeNames = true)
class User {
    Integer id
    Boolean	is_bot
    String	first_name
    String	last_name
    String	username
    String	language_code
}
