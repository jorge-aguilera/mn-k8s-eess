package telegram

import groovy.transform.ToString

@ToString(includeNames = true)
class Update {

    int update_id

    UserMessage message

    CallbackQuery callback_query

}
