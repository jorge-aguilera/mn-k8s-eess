package telegram
import groovy.transform.ToString

@ToString(includeNames = true)
class KeyboardButton {
    String text
    boolean request_contact = false
    boolean request_location = false

}
