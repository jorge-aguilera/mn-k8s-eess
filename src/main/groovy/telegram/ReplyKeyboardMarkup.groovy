package telegram
import groovy.transform.ToString

@ToString(includeNames = true)
class ReplyKeyboardMarkup extends ReplyMarkup{

    List<List<KeyboardButton>> keyboard = []

}
