package telegram

import io.micronaut.http.MediaType
import io.micronaut.http.client.multipart.MultipartBody

class SendAnimation {
    String chat_id
    String caption
    byte[] bytes

    File file
    void getFile(){
        throw new RuntimeException("no file needed")
    }

    void setFile( File f){
        caption = f.name.split('\\.').dropRight(1).join(' ')
        bytes = f.bytes
    }

    MultipartBody multipartBody(){
        return MultipartBody.builder()
                .addPart("chat_id", chat_id)
                .addPart("caption", caption ?:'')
                .addPart('animation', (caption ?:'animation')+'.gif', MediaType.APPLICATION_OCTET_STREAM_TYPE, bytes)
                .build()
    }

}
