package mn.eess

import groovy.util.logging.Log
import io.reactivex.Single
import telegram.InlineKeyboardMarkup
import telegram.Message
import telegram.ReplyMarkup
import telegram.SendAudio
import telegram.TelegramClient

import javax.inject.Singleton

@Log
@Singleton
class TelegramService {

    TelegramClient telegramClient

    TelegramService(TelegramClient telegramClient){
        this.telegramClient = telegramClient
    }


    void sendMessage(String chatId, String text){
        sendMessage( new Message(
                chat_id: chatId,
                text: text
        ))
    }

    void sendMessage(Message message){
        log.info "sending $message"
        telegramClient.sendMessage(message).subscribe()
    }

    void sendLocation(String chatId, float latitude, float longitude){
        log.info "sending location $latitude $longitude"
        telegramClient.sendLocation(chatId, latitude, longitude).subscribe()
    }


    void sendAudio(SendAudio audio){
        log.info "sending audio"
        telegramClient.sendAudio(audio.multipartBody()).subscribe()
    }

    void editMessageReplyMarkup(String chat_id,  int message_id, ReplyMarkup keyboardMarkup){
        telegramClient.editMessageReplyMarkup(chat_id, message_id,keyboardMarkup).subscribe()
    }

    void editMessageReplyMarkup(String inline_message_id, ReplyMarkup keyboardMarkup){
        telegramClient.editMessageReplyMarkup(inline_message_id,keyboardMarkup).subscribe()
    }

    void notAllowed(String chatId) {
        Single.create{ emitter->
            sendMessage(chatId, """Lo siento pero este Bot está todavía en desarrollo y el acceso está restringuido
a usuarios de pruebas

Made with 💙 by Puravida Software 
Usando los catálogos de datos abiertos del Gobierno de España
""")
            emitter.onSuccess(true)
        }.subscribe()
    }

    void answerCallbackQuery(String callback_query_id, String text, boolean show_alert, int cache_time){
        telegramClient.answerCallbackQuery(callback_query_id,text, show_alert, cache_time).subscribe()
    }

}
