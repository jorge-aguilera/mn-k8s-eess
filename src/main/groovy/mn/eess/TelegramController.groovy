package mn.eess

import groovy.util.logging.Log
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.reactivex.Single
import mn.eess.dialog.ChangePriceService
import mn.eess.dialog.DialogService
import telegram.Chat
import telegram.From
import telegram.Update
import telegram.UserMessage

import javax.annotation.Nullable
import javax.inject.Inject

@Log
@Controller('${telegram.token}')
class TelegramController {

    DialogService dialogService

    TelegramController(DialogService dialogService){
        this.dialogService = dialogService
    }

    @Post
    String index( Update update){
        log.info "$update"
        Single.create{ emitter ->
            dialogService.onMessage(update)
            emitter.onSuccess( true)
        }.subscribe()

        "done"
    }

    @Get('/')
    String test(){
        Update update = new Update(
                message: new UserMessage(
                        from: new From(
                                id: 0
                        ),
                        chat: new Chat(
                                id: '0'
                        )
                ),
        )
        dialogService.onMessage(update)
    }

    @Inject
    ChangePriceService changePriceService

    @Get('/check{/session}')
    Map<String, String> checkPrices(@Nullable String session){
        changePriceService.checkSessions(session)
    }

    @Inject
    TelegramService telegramService

    @Post('/spam')
    void spam( @Body Spam spam){
        telegramService.sendMessage(spam.channel, spam.message)
    }

}
