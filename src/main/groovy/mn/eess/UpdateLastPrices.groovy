package mn.eess

import io.micronaut.context.event.ApplicationEventListener
import io.micronaut.discovery.event.ServiceStartedEvent
import mn.eess.services.EESSService
import mn.eess.services.SessionService

import javax.inject.Inject

class UpdateLastPrices implements ApplicationEventListener<ServiceStartedEvent> {

    @Inject
    EESSService eessService

    @Inject
    SessionService sessionService

    void onApplicationEvent(ServiceStartedEvent event) {
        eessService.init()
    }

}
