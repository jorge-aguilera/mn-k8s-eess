package mn.eess.dialog

import groovy.util.logging.Log
import io.micronaut.context.annotation.Value
import mn.eess.TelegramService
import mn.eess.model.Estacion
import mn.eess.model.Session
import mn.eess.model.Sugerencia
import mn.eess.services.EESSService
import mn.eess.services.SessionService
import mn.eess.services.util.SpeechParser
import telegram.InlineKeyboardButton
import telegram.InlineKeyboardMarkup
import telegram.KeyboardButton
import telegram.Message
import telegram.ReplyKeyboardMarkup
import telegram.ReplyKeyboardRemove
import telegram.ReplyMarkup
import telegram.Update

import javax.inject.Singleton

@Log
@Singleton
class DialogService {

    SessionService sessionService

    EESSService eessService

    TelegramService telegramService

    @Value('${telegram.stadistic}')
    String channel

    DialogService(SessionService sessionService, EESSService eessService, TelegramService telegramService){
        this.sessionService = sessionService
        this.eessService = eessService
        this.telegramService = telegramService
    }

    void onMessage(Update update) {
        try{
            Session session = processMessage(update)
            notifyAdmin(session)
        }catch( e ){
            log.severe "Error $e"
            e.printStackTrace()
        }
    }

    void notifyAdmin(Session session){
        telegramService.sendMessage(channel,"${session.name ?: session.id} está usando EESS para $session.carburante")
    }

    Session processMessage(Update update){

        int userId = update.callback_query?.from ?
                update.callback_query.from.id : update.message.from.id

        String chatId = update.message ?
                update.message.chat.id : update.callback_query.message.chat.id

        if( sessionService.isAllowed(userId) == false){
            telegramService.sendMessage(notAllowed(chatId))
            return
        }

        Session session = sessionService.findSessionById(chatId)
        if( !session ){
            String name = update.message?.from.username
            session = new Session(id: chatId, name:name)
            sessionService.saveSession(session)
        }

        String[] args = ['error']

        if( update.message?.location){
            args = ['location', "$update.message.location.latitude", "$update.message.location.longitude"]
        }else{
            if( update.callback_query?.data ){
                args = update.callback_query.data.split((' '))
            }else {
                if (update.message?.text) {
                    args = update.message.text.split((' '))
                }
            }
        }

        String cmd = args[0].toLowerCase()
        switch ( cmd ) {
            case '/start':
                session = new Session(id:chatId)
                break
            case 'location':
                session.latitude = args[1]
                session.longitude = args[2]
                break

            case '/carburante':
                telegramService.sendMessage(menuCarburante(session))
                return session

            case '/location':
                telegramService.sendMessage(menuWeb(session))
                return session

            case 'info_estacion':
                Estacion estacion = eessService.findEESSById(args[1])
                if(update.callback_query?.inline_message_id)
                    telegramService.editMessageReplyMarkup( update.callback_query.inline_message_id, replaceWithInfo(session, estacion) )
                else
                    telegramService.editMessageReplyMarkup( chatId, update.callback_query.message.message_id, replaceWithInfo(session, estacion) )
                return session

            case 'send_localization':
                Estacion estacion = eessService.findEESSById(args[1])
                telegramService.sendLocation(session.id, estacion.latitude, estacion.longitude)
                return session

            case 'save_estacion':
                Estacion estacion = eessService.findEESSById(args[1])
                session.estacionPreferida = estacion
                sessionService.saveSession(session)
                telegramService.answerCallbackQuery(update.callback_query.id,
                        "${estacion.marca.split(' ').first()} guardada como favorita", false, 10)
                return session

            case 'remove_estacion':
                session.estacionPreferidaId = null
                sessionService.saveSession(session)
                telegramService.answerCallbackQuery(update.callback_query.id,
                        "No tienes ninguna estación como favorita", false, 10)
                return session

            case 'back':
                List<Sugerencia> oldSugerencias = eessService.suggest(session, 4)
                if(update.callback_query?.inline_message_id)
                    telegramService.editMessageReplyMarkup( update.callback_query.inline_message_id, buildSuggestKeyboard(session, oldSugerencias) )
                else
                    telegramService.editMessageReplyMarkup( chatId, update.callback_query.message.message_id, buildSuggestKeyboard(session, oldSugerencias) )
                return session

            case 'cancel':
            case 'close_menu':
                if(update.callback_query?.inline_message_id)
                    telegramService.editMessageReplyMarkup( update.callback_query.inline_message_id,  new InlineKeyboardMarkup())
                else
                    telegramService.editMessageReplyMarkup( chatId, update.callback_query.message.message_id, new InlineKeyboardMarkup() )
                return session

            case '/favorita':
                telegramService.sendMessage(menuFavorita(session))
                return session

            default:
                if( update.message?.text){
                    String carburante = Estacion.tiposCarburante.find {
                        it == update.message.text
                    }
                    if( carburante ){
                        session.carburante = carburante
                    }else{
                        telegramService.sendMessage(sayHello(session))
                        return session
                    }
                }else{
                    telegramService.sendMessage(sayHello(session))
                    return session
                }
        }

        sessionService.saveSession(session)
        log.info "Session $session saved"

        if( !session.carburante ){
            telegramService.sendMessage(sayHello(session))
            return session
        }

        if( !session.latitude ){
            telegramService.sendMessage(askLocation(session))
            return session
        }

        List<Sugerencia> sugerencias = eessService.suggest(session, 4)

        if( ! sugerencias ){
            telegramService.sendMessage(session.id, "Ufff, lo tienes chungo, no encuentro nada cerca de ti para ${session.carburante}")
            return session
        }

        telegramService.sendMessage(suggest(session, sugerencias))

        session
    }

    Message notAllowed(String chatId) {
        new Message(
                chat_id:  chatId,
                text:  """Ahora mismo me pillas fatal. Estoy en medio de una actualización de software. 
Prueba un poco más tarde por favor.

Made with 💙 by Puravida Software 
Usando los catálogos de datos abiertos del Gobierno de España
"""
        )
    }

    Message sayHello(Session session){
        new Message(
                chat_id: session.id,
                text:"""
Hola, soy un bot que puede ayudarte a buscar la Estacion de Servicio de carburante más barata según tu posición

En primer lugar necesito saber qué tipo de carburante usas. Puedes utilizar el menú rápido siguiente. 

Recuerda que en cualquier momento puedes elegir otro tipo de carburante mediante el comando */carburante*

Una vez que hayas localizado una estación puedes marcarla como favorita y consultarla directamente
con el comando */favorita*

Made with 💙 by Puravida Software 
Usando los catálogos de datos abiertos del Gobierno de España
""",
                reply_markup: buildKeyboard()
        )
    }

    Message menuCarburante(Session session){
        new Message(
                chat_id: session.id,
                text:"""Actualmente tienes seleccionado *${session.carburante}* . Utiliza el menú para elegir otro tipo""",
                reply_markup: buildKeyboard()
        )
    }

    Message menuWeb(Session session){
        new Message(
                chat_id: session.id,
                text:"""Actualmente tienes seleccionado *${session.carburante}* . Utiliza el menú para elegir otro tipo""",
                reply_markup: buildKeyboard()
        )
    }

    Message askLocation(Session session){
        new Message(
                chat_id: session.id,
                text:"""Ok, he guardado *${session.carburante}* como tu carburante de referencia

Cada vez que me envies tu posicion buscaré estaciones con el mejor precio para ${session.carburante} cerca de tí.
""",
                reply_markup: new ReplyKeyboardRemove()
        )
    }


    Message suggest(Session session, List<Sugerencia> sugerencias){
        new Message(
                chat_id: session.id,
                text:"""Precios de *${session.carburante}* 
Selecciona una estación para más info""",
                reply_markup: buildSuggestKeyboard(session, sugerencias)
        )
    }

    ReplyMarkup buildSuggestKeyboard(Session session, List<Sugerencia> sugerencias){
        SpeechParser speechParser = new SpeechParser()
        List<InlineKeyboardButton> buttons = []
        sugerencias.each{ Sugerencia sugerencia->
            buttons.add(
                    new InlineKeyboardButton(
                            text: speechParser.buildShortText(session,sugerencia),
                            callback_data: "info_estacion $sugerencia.estacion.id"
                    )
            )
        }
        buttons.add(
                new InlineKeyboardButton(
                        text: "Cerrar menú",
                        callback_data: "close_menu"
                )
        )
        List<List<InlineKeyboardButton>> keyboard = buttons.collate(1)
        new InlineKeyboardMarkup(inline_keyboard: keyboard)
    }

    InlineKeyboardMarkup replaceWithInfo(Session session, Estacion estacion){
        List<InlineKeyboardButton> buttons = []
        buttons.add(
                new InlineKeyboardButton(
                        text: "Localizar en mapa ${estacion.marca.split(' ').first()}",
                        callback_data: "send_localization $estacion.id"
                )
        )
        buttons.add(
                new InlineKeyboardButton(
                        text: "Guardar ${estacion.marca.split(' ').first()} como favorita",
                        callback_data: "save_estacion $estacion.id"
                )
        )
        buttons.add(
                new InlineKeyboardButton(
                        text: "<- Volver al listado",
                        callback_data: "back $estacion.id"
                )
        )
        List<List<InlineKeyboardButton>> keyboard = buttons.collate(1)
        new InlineKeyboardMarkup(inline_keyboard: keyboard)
    }

    ReplyMarkup buildKeyboard(){
        List<String> tipos = Estacion.tiposCarburante

        List<KeyboardButton> buttons = []

        tipos.each{ String txt ->
            buttons.add new KeyboardButton(
                    text: txt
            )
        }
        List<List<KeyboardButton>> keyboard = buttons.collate(2)
        new ReplyKeyboardMarkup(keyboard: keyboard)
    }

    Message menuFavorita(Session session){
        if( "${session.estacionPreferidaId ?: 0}" as int){
            Estacion estacion = eessService.findEESSById(session.estacionPreferidaId)
            String text = "Actualmente tienes *${estacion.marca}* en $estacion.direccion como favorita"

            if( session.carburante ) {
                text += "\n${session.carburante} está a *${estacion.getPrecioCarburanteAsString(session.carburante)}*"
            }
            return new Message(
                    chat_id: session.id,
                    text:text,
                    reply_markup: buildMenuFavoritaKeyboard(estacion)
            )
        }
        return new Message(
                chat_id: session.id,
                text:"No tienes ninguna estación marcada como favorita. Localiza una y marcala como favorita",
        )
    }

    InlineKeyboardMarkup buildMenuFavoritaKeyboard(Estacion estacion){
        List<InlineKeyboardButton> buttons = []
        buttons.add(
                new InlineKeyboardButton(
                        text: "Quitar ${estacion.marca.split(' ').first()} como favorita",
                        callback_data: "remove_estacion $estacion.id"
                )
        )
        buttons.add(
                new InlineKeyboardButton(
                        text: "Cancelar",
                        callback_data: "cancel"
                )
        )
        List<List<InlineKeyboardButton>> keyboard = buttons.collate(1)
        new InlineKeyboardMarkup(inline_keyboard: keyboard)
    }
}
