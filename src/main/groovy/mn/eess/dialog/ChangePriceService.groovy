package mn.eess.dialog

import groovy.util.logging.Log
import mn.eess.TelegramService
import mn.eess.model.Estacion
import mn.eess.model.Session
import mn.eess.services.EESSService
import mn.eess.services.SessionService
import telegram.Message

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@Log
class ChangePriceService {

    @Inject
    SessionService sessionService

    @Inject
    EESSService eessService

    @Inject
    TelegramService telegramService

    Map<String, String> checkSessions(String session=null){
        Map<String, String> ret = [:]
        sessionService.listSessions().inject(ret){ Map<String,String> map,  Session it->
            println "searching for $it.id"
            if( !session || it.id == session) {
                map[it.id] = checkSession(it)
            }
            map
        }
        ret
    }

    String checkSession(Session session){
        try {
            if (!session.estacionPreferidaId)
                return "sin estacion preferida"
            if (!session.precioEstacion)
                return "sin precio estacion"
            if (!session.carburante)
                return "sin carburante"

            Estacion estacion = eessService.findEESSById(session.estacionPreferidaId)
            log.info "chequeando para $session?.name $estacion?.direccion"
            if( !estacion ){
                return "no existe la estacion"
            }
            float precio = estacion.getPrecioCarburante(session.carburante)
            if (precio == session.precioEstacion)
                return "sin cambio de precio"

            String msg = """ Hola ${session.name ?: ''}
${precio > session.precioEstacion ? '*Malas noticias* 😭😭' : '*Buenas noticias* 😀😀'}
Nuevo precio *${estacion.getPrecioCarburanteAsString(session.carburante)}* para ${session.carburante}
${precio > session.precioEstacion ? 'Sube' : 'Baja'} ${Math.abs(precio - session.precioEstacion).round(3)} euro
($estacion.marca, $estacion.direccion)
"""
            telegramService.sendMessage(new Message(
                    chat_id: session.id,
                    text: msg
            ))
            String ret = "Notificado el cambio de precio de $session.precioEstacion a $precio"
            session.precioEstacion = precio
            sessionService.saveSession(session)
            log.info ret
            return ret
        }catch(Exception e){
            return e.message
        }
    }

}
