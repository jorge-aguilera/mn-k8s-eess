package mn.eess.model

import groovy.transform.ToString

@ToString(includeNames = true)
class Sugerencia implements Serializable{

    String id
    String sessionId

    float distanceEstacion
    Estacion estacion

}
