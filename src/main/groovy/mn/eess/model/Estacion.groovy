package mn.eess.model

import groovy.transform.CompileStatic
import groovy.transform.ToString
import groovy.transform.TypeCheckingMode

@CompileStatic(TypeCheckingMode.SKIP)
@ToString(includeNames = true)
class Estacion implements Serializable{

    String id

    Float hidrogeno

    Float biodiesel
    Float bioetanol
    Float gasnaturalcomprimido
    Float gasnaturallicuado
    Float gasnaturallicuadopetroleo

    Float gasoleoa
    Float gasoleob
    Float gasoleopremium

    Float gasolina95e10
    Float gasolina95e5
    Float gasolina95e5premium

    Float gasolina98e10
    Float gasolina98e5

    String direccion
    String marca

    float latitude
    float longitude

    String carburanteProperty(String carburante) {
        String property = 'gasolina95e5'
        switch( carburante ) {
            case '95-E5':
                property = 'gasolina95e5'
                break
            case '98-E5':
                property = 'gasolina98e5'
                break
            case '95-E10':
                property = 'gasolina95e10'
                break
            case '98-E10':
                property = 'gasolina98e10'
                break
            case '95-Premium':
                property = 'gasolina95e5premium'
                break
            case 'Diesel-B7':
                property = 'gasoleoa'
                break
            case 'Diesel-B10':
                property = 'gasoleob'
                break
            case 'Diesel-XTL':
                property = 'gasoleopremium'
                break
            case 'GNC':
                property = 'gasnaturalcomprimido'
                break
            case 'LPG':
                property = 'gasnaturallicuado'
                break
            case 'LNG':
                property = 'gasnaturallicuadopetroleo'
                break
            case 'BioDiesel':
                property = 'biodiesel'
                break
            case 'BioEtanol':
                property = 'bioetanol'
                break
            case 'Hidrogeno':
                property = 'hidrogeno'
                break
        }
        property
    }

    Float getPrecioCarburante(String carburante){
        String property = carburanteProperty(carburante)
        this."$property" ?: 0
    }

    String getPrecioCarburanteAsString(String carburante){
        String property = carburanteProperty(carburante)
        precioToString(this."$property" ?: 0)
    }

    static String precioToString( float precio){
        String str = precio as String
        str = str.replace('.',',')
        "${str.take(5)}€"
    }

    static List<String>getTiposCarburante(){
        [
                '95-E5',
                '98-E5',
                '95-E10',

                //'98-E10',
                //'95-Premium',

                'Diesel-B7',
                'Diesel-B10',
                'Diesel-XTL',

                'GNC',
                'LPG',
                'LNG',
                'BioDiesel',
                'BioEtanol',
                'Hidrogeno',
        ]
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    static Estacion fromNode(node ){
        new Estacion(
                id : node.IDEESS.text(),

                direccion : node.Dirección.text() ?: "",
                marca : node.Rótulo.text() ?: "",

                latitude: node.Latitud.text().replace(',', '.') as float,
                longitude: node.Longitud_x0020__x0028_WGS84_x0029_.text().replace(',', '.') as float,

                hidrogeno : stringToPrice(node.Precio_x0020_Hidrogeno.text()),

                biodiesel : stringToPrice(node.Precio_x0020_Biodiesel.text()),
                bioetanol : stringToPrice(node.Precio_x0020_Bioetanol.text()),
                gasnaturalcomprimido : stringToPrice(node.Precio_x0020_Gas_x0020_Natural_x0020_Comprimido.text()),
                gasnaturallicuado : stringToPrice(node.Precio_x0020_Gas_x0020_Natural_x0020_Licuado.text()),
                gasnaturallicuadopetroleo : stringToPrice(node.Precio_x0020_Gases_x0020_licuados_x0020_del_x0020_petróleo.text()),

                gasoleoa : stringToPrice(node.Precio_x0020_Gasoleo_x0020_A.text()),
                gasoleob : stringToPrice(node.Precio_x0020_Gasoleo_x0020_B.text()),
                gasoleopremium : stringToPrice(node.Precio_x0020_Gasoleo_x0020_Premium.text()),

                gasolina95e10 : stringToPrice(node.Precio_x0020_Gasolina_x0020_95_x0020_E10.text()),
                gasolina95e5 : stringToPrice(node.Precio_x0020_Gasolina_x0020_95_x0020_E5.text()),
                gasolina95e5premium : stringToPrice(node.Precio_x0020_Gasolina_x0020_95_x0020_E5_x0020_Premium.text()),

                gasolina98e10 : stringToPrice(node.Precio_x0020_Gasolina_x0020_98_x0020_E10.text()),
                gasolina98e5 : stringToPrice(node.Precio_x0020_Gasolina_x0020_98_x0020_E5.text()),
        )
    }

    static Float stringToPrice(String txt){
        if( ! txt )
            return null
        txt.replaceAll(',','.') as float
    }
}
