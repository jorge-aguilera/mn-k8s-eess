package mn.eess.model

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import groovy.transform.ToString
import groovy.transform.TypeCheckingMode

@ToString(includeNames = true)
class Session implements Serializable{

    String id
    String name

    String marca
    String carburante

    String latitude
    String longitude

    String estacionPreferidaId
    float precioEstacion

    void setEstacionPreferida(Estacion estacion){
        estacionPreferidaId = estacion.id
        precioEstacion = estacion.getPrecioCarburante(carburante)
    }

    String toJson(){
        JsonOutput.toJson([
                id:this.id,
                name:this.name,
                marca:this.marca,
                carburante:this.carburante,
                latitude:this.latitude,
                longitude:this.longitude,
                estacionPreferidaId: this.estacionPreferidaId,
                precioEstacion : this.precioEstacion
        ])
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    static Session fromJson(String json){
        def obj = new JsonSlurper().parseText(json)
        Session ret = new Session(
                id: obj.id,
                name: obj.name,
                marca: obj.marca,
                carburante: obj.carburante,
                latitude: obj.latitude,
                longitude: obj.longitude,
                estacionPreferidaId: obj.estacionPreferidaId ?: "0",
                precioEstacion: (obj.precioEstacion ?: "0") as float
        )
        ret
    }

}
