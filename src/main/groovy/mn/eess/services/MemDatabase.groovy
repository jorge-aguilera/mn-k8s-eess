package mn.eess.services

import groovy.time.BaseDuration
import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import mn.eess.model.Estacion

@Log
class MemDatabase {

    static final String url = "https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/"

    List<Estacion> bbdd = []

    Date last

    @CompileStatic(TypeCheckingMode.SKIP)
    @Synchronized
    void init(){

        if( last ) {
            if (groovy.time.TimeCategory.minus(new Date(), last).hours > 1) {
                bbdd = []
            }
        }

        if( bbdd.size() )
            return

        try {
            String xml = new InputStreamReader(url.toURL().openStream(), 'UTF-8').text

            new XmlParser().parseText(xml).ListaEESSPrecio.EESSPrecio.each { eess ->
                bbdd.add Estacion.fromNode(eess)
            }
            log.info "prices updated"
        }catch(e){
            log.severe(e.toString())
        }
        last = new Date()
    }

    Estacion findEESSById(String id){
        init()
        bbdd.find{ it.id == id }
    }

}
