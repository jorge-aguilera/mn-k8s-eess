package mn.eess.services

import mn.eess.model.Session

interface SessionService {

    boolean isAllowed(int userId)

    Session findSessionById(String sessionId )

    Session saveSession( Session session)

    void deleteSession(Session session)

    List<Session> listSessions()
}
