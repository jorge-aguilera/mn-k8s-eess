package mn.eess.services

import groovy.transform.Synchronized
import mn.eess.model.Estacion
import mn.eess.model.Session
import mn.eess.model.Sugerencia
import mn.eess.services.util.BestLocateService

import javax.annotation.PostConstruct
import javax.inject.Singleton

@Singleton
class EESSService {

    MemDatabase memDatabase

    //@PostConstruct
    void init(){
        memDatabase = new MemDatabase()
        memDatabase.init()
    }

    Estacion findEESSById(String id){
        memDatabase.findEESSById(id)
    }

    @Synchronized
    List<Sugerencia> suggest(Session session, int size=2){

        BestLocateService bestLocateService = new BestLocateService(bbdd: memDatabase.bbdd)

        List<Sugerencia> ret = bestLocateService.suggest(session.latitude as float, session.longitude as float,  session.carburante, size, session.marca)

        ret = ret.sort{ a, b ->
            a.estacion.getPrecioCarburante(session.carburante) <=> b.estacion.getPrecioCarburante(session.carburante)
        }

        ret*.sessionId = session.id
        ret
    }

}
