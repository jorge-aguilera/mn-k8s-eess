package mn.eess.services.util

import mn.eess.model.Estacion
import mn.eess.model.Sugerencia


class BestLocateService {

    List<Estacion> bbdd

    List<Sugerencia> suggest(float latitude, float longitude, String carburante=null, int size=1, String marca=null) {

        List<Estacion> list = bbdd.findAll{ Estacion estacion ->
            if( carburante )
                return estacion.getPrecioCarburante(carburante) > 0
            true
        }

        list.sort{ eess ->
            metersTo( eess.latitude, eess.longitude, latitude, longitude)
        }

        List<Estacion> tmp = []

        if( marca ){
            marca = marca.toLowerCase()
            tmp.addAll list.findAll{ eess ->
                eess.marca.toLowerCase().indexOf(marca) != -1
            }
        }

        if( tmp.size() ){
            list = tmp
        }

        List<Sugerencia> ret = [] as List<Sugerencia>
        list.take(size).each{ Estacion estacion ->
            Sugerencia sugerencia = new Sugerencia(
                    id: UUID.randomUUID().toString(),
                    estacion: estacion,
                    distanceEstacion: metersTo( estacion.latitude, estacion.longitude, latitude, longitude),
            )
            ret.add sugerencia
        }

        ret
    }

    static float metersTo(float lat1, float lng1, float lat2, float lng2) {
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        double meters = radioTierra * va2;
        meters as float;
    }

}
