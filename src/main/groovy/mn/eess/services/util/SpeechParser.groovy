package mn.eess.services.util

import groovy.util.logging.Log
import mn.eess.model.Estacion
import mn.eess.model.Session
import mn.eess.model.Sugerencia

@Log
class SpeechParser {


    String buildText(Session session, Sugerencia sugerencia){

        Estacion estacion = sugerencia.estacion
        String precio = estacion.getPrecioCarburante(session.carburante)
        precio = precio.replace('.',',')

        String[]precios = precio.split(',')
        precios[1] = precios[1].take(2)

        String distance = "$sugerencia.distanceEstacion"
        String[] distances = distance.split('\\.')


        log.info "$distances"
        if( distances[0] as int == 0){
            int metros = distances[1].take(3) as int
            distance = " $metros metros"
        }else{
            distance = "${distances[0]} kilometro${ (distances[0] as int) > 1 ? 's' : ''}".trim()
            if( (distances[1] as int) > 100){
                int metros = distances[1].take(3) as int
                distance += " $metros metros"
            }
        }

        [
            "A ${distance} ${estacion.marca} tiene el precio de $session.carburante ",
            "a ${precios[0]} euro${ (precios[0] as int) > 1 ? 's' : ''} ",
            "con ${precios[1]} centimo${ (precios[1] as int) > 1 ? 's' : ''} ",
        ].join(' ')
    }

    String buildShortText(Session session, Sugerencia sugerencia){

        Estacion estacion = sugerencia.estacion

        String precio = estacion.getPrecioCarburante(session.carburante)
        precio = precio.replace('.',',')

        String distance = "$sugerencia.distanceEstacion"
        String[] distances = distance.split('\\.')

        int km = distances[0] as int
        int metros = distances[1].take(3) as int
        if( km == 0){
            distance = " $metros mt"
        }else{
            if( metros > 600) {
                km++
            }
            distance = " $km km${ km > 1 ? 's' : ''}".trim()
        }

        "${precio.take(5)}€ - ${distance} (${estacion.marca.split(' ').first()})"
    }

}
