package mn.eess.services
import groovy.util.logging.Log
import io.micronaut.context.annotation.Requires
import io.micronaut.context.annotation.Value
import io.micronaut.context.env.Environment
import mn.eess.model.Session

import javax.annotation.PostConstruct
import javax.inject.Singleton
import java.nio.ByteBuffer

@Log
@Singleton
@Requires(notEnv = Environment.GOOGLE_COMPUTE)
class FileSessionService implements SessionService{

    @Value('${storage.file-folder}')
    String clientFolder

    File rootFolder

    @PostConstruct
    void init(){
        rootFolder = new File(clientFolder)
        boolean created = rootFolder.mkdirs()
        try {
            log.info(rootFolder.absolutePath)
            File test = new File(rootFolder, "test.txt")
            test.text = "hola"
        }catch(e){
            e.printStackTrace()
        }
        log.info("FileSessionCreated "+ created)
    }

    @Override
    boolean isAllowed(int userId) {
        return true
    }

    @Override
    List<Session> listSessions() {
        List<Session> ret = []
        rootFolder.eachFile {
            if( it.name.endsWith('.json')){
                Session add = Session.fromJson(it.text)
                ret.add(add)
            }
        }
        ret
    }

    @Override
    Session findSessionById(String sessionId) {
        File f = new File(rootFolder,"${sessionId}.json")
        if( f.exists() == false ){
            return null
        }
        String json = f.text
        log.info "Session json $json"
        Session ret = Session.fromJson(json)
        log.info "return $json $ret"
        ret
    }

    @Override
    Session saveSession(Session session) {
        File f = new File(rootFolder,"${session.id}.json")
        f.bytes = session.toJson().bytes
        session
    }

    @Override
    void deleteSession(Session session) {
        File f = new File(rootFolder,"${session.id}.json")
        if( f.exists())
            f.delete()
    }
}
